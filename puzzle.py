def search(values, output):
    from operator import add, mul, div, sub
    from itertools import combinations
    from collections import namedtuple
    operation = namedtuple('operation', 'func symbol commutative')
    ops = [operation(div, "/", False), operation(mul, "x", True), operation(add, "+", True), operation(sub, "-", False)]
    expression = namedtuple('expression', 'val exp')

    def __search_internal(output_val, unprocessed_exp):
        if unprocessed_exp and len(unprocessed_exp) == 1:
            return unprocessed_exp[0].exp if unprocessed_exp[0].val == output_val else None
        exp_combination_iter = combinations(unprocessed_exp, 2)
        for exp_combination in exp_combination_iter:
            exp1, exp2 = exp_combination[0], exp_combination[1]
            remaining_unprocessed_exp = filter(lambda x: x is not exp1 and x is not exp2, unprocessed_exp)
            for op in ops:
                ret_val = __search_recursive(op, exp1, exp2, output_val, remaining_unprocessed_exp) or \
                          (__search_recursive(op, exp2, exp1, output_val, remaining_unprocessed_exp)
                           if not op.commutative else None)
                if ret_val:
                    return ret_val

    def __search_recursive(op, exp1, exp2, output_val, unprocessed_exp):
        if not (op.func is div and exp2.val == 0):
            intermediate_exp = expression(op.func(exp1.val, exp2.val), "(" + exp1.exp + op.symbol + exp2.exp + ")")
            unprocessed_exp.append(intermediate_exp)
            return __search_internal(output_val, unprocessed_exp) or unprocessed_exp.remove(intermediate_exp)

    expressions = [expression(v * 1.0, str(v)) for v in values]
    return __search_internal(output, list(expressions))

# Testing code
if __name__ == '__main__':
    val = [1, 3, 4, 6]
    result = 24
    print "values:", val, "output:", result, "expression:", search(val, result) or "No Solution Exist"
    val = [4, 6, 8, 8]
    result = 28
    print "values:", val, "output:", result, "expression:", search(val, result) or "No Solution Exist"
    val = [3, 3, 7, 7]
    result = 24
    print "values:", val, "output:", result, "expression:", search(val, result) or "No Solution Exist"
    val = [3, 3, 7, 7]
    result = 55
    print "values:", val, "output:", result, "expression:", search(val, result) or "No Solution Exist"
    val = [2, 4, 7, 8]
    result = 44
    print "values:", val, "output:", result, "expression:", search(val, result) or "No Solution Exist"
    val = [3, 3, 6, 9]
    result = 25
    print "values:", val, "output:", result, "expression:", search(val, result) or "No Solution Exist"
    val = [3, 4, 7, 8]
    result = 10
    print "values:", val, "output:", result, "expression:", search(val, result) or "No Solution Exist"
